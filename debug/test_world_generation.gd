extends Node2D

func _ready() -> void:
	# Create game engine
	var engine = GameEngine.new(1)
	# Note: Seed = 13 has issue where link goes through planet!
	
	# Set players
	engine.players = ['p1', 'p2', 'p3', 'p4']
	
	# Generate starting locations
	engine.gen_starting_locations()
	
	# Generate planets
	engine.gen_planets(40)
	
	# Generate links
	engine.gen_links()
	
	# Render
	var render = GameRender.new(engine)
	self.add_child(render)
	render.render()
	
	# Enable camera
	$Camera.interactive = true
