extends Node2D

# Load scenes
const ui_scene = preload("res://scenes/UserInterface.tscn")

func _ready() -> void:
	# Define new game info
	var players = ['p1', 'p2', 'p3', 'p4']
	var iscomputer = [false, false, false, false]
	var seed = 1
	
	# Create game engine
	var engine = GameEngine.new(1)
	engine.new_game(players, seed)

	# Render
	var render = GameRender.new(engine)
	self.add_child(render)
	render.render()
	
	# Enable camera
	$Camera.interactive = true
	
	# Add user interface
	var ui = ui_scene.instantiate()
	self.add_child(ui)
