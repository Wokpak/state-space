class_name AIPlayer extends Node

const mcts_class = preload("res://mcts/MCTS.gd")

var player_name : String
var active_player : String
var mcts : MCTS = null

func _ready():
	pass

func _process(delta):
	self.take_turn()

func _on_new_active_player(active_player):
	self.active_player = active_player

func _on_turn_taken(action):
	"""Update MCTS for new game state following action"""
	var mcts_old = self.mcts

	# Get new mcts node
	self.mcts = self.mcts.update_root(action)

	# Check its not self
	if self.mcts == mcts_old:
		return

	# Remove old mcts
	mcts_old.remove_child(self.mcts)
	mcts_old.queue_free()

	# Add new mcts
	self.add_child(self.mcts)

func take_turn():
	"""Select action using mcts"""
	var engine = get_parent()

	# Initialise MCTS
	if self.mcts == null:
		# Get game engine state
		self.mcts = mcts_class.new(player_name, null)
		self.mcts.rootstate = engine
		self.mcts.player = self.player_name
		self.add_child(self.mcts)

	# Search
	var turn = self.mcts.search()

	# Continue searching
	if turn == null:
		return

	# Take turn
	if len(turn) == 2:
		engine.take_turn(turn[0], turn[1])
	if len(turn) == 4:
		engine.take_turn(turn[0], turn[1], turn[2], turn[3])
