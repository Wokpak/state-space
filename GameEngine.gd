class_name GameEngineOld extends Node2D

# Load classes
const NameGenerator = preload("res://NameGenerator.gd")
const solar_scene = preload("res://SolarSystem.tscn")
const planet_scene = preload("res://Planet.tscn")
const aiplayer_scene = preload("res://AIPlayer.tscn")

# Define variables
var players : Array
var colours : Dictionary = {'Neutral': Color(1, 1, 1)}
var active_player : String
var round_num : int
var solar_system : SolarSystem
var rng : RandomNumberGenerator

# Define signals
signal turn_taken
signal new_active_player

func _ready():
	pass

func clone() -> GameEngine:
	"""Custom duplication script"""
	# Duplicate script
	var clone = get_script().new()
	# Set properties
	clone.players = self.players.duplicate(true)
	clone.colours = self.colours
	clone.active_player = self.active_player
	clone.round_num = self.round_num
	clone.solar_system = self.solar_system.clone()
	# Set rng
	clone.rng = RandomNumberGenerator.new()
	clone.rng.seed = self.rng.seed
	clone.rng.state = self.rng.state
	# Add children
	clone.add_child(clone.solar_system)
	return clone

#func _process(delta):
#	print(self.rng.seed)

# ---------------------------

func new_game(players : Array, colours : Array, game_seed : int, iscomputer : Array):
	"""Start new game"""
	self.visible = true

	# Set up player states
	self.players = players
	self.active_player = players[0]

	# Set up player colours
	for ii in range(0, len(players)):
		self.colours[players[ii]] = colours[ii]

	# Initialise random number generator for game
	self.rng = RandomNumberGenerator.new()
	self.rng.set_seed(game_seed)

	# Generate solarsystem for given seed
	solar_system = solar_scene.instantiate()
	add_child(solar_system)
	self.gen_planets()
	self.gen_system()
	self.gen_starting_locations()

	# Initialise random number generator for game
	self.rng.seed = randi()

	# Link client to planets
	var client = get_tree().get_root().get_node('Client')
	self.connect('new_active_player',Callable(client,'_on_new_active_player'))
	# Link client to planets
	var planets = solar_system.get_planets()
	for planet in planets:
		planet.link_client(client)

	# Set up AI
	for ind in range(0, len(iscomputer)):
		if iscomputer[ind] == true:
			# Add AI
			var aiplayer = aiplayer_scene.instantiate()
			aiplayer.player_name = self.players[ind]
			self.connect('turn_taken',Callable(aiplayer,'_on_turn_taken'))
			self.connect('new_active_player',Callable(aiplayer,'_on_new_active_player'))
			self.add_child(aiplayer)

	# Start game
	self.round_num = 0
	emit_signal('new_active_player', self.active_player)

func gen_planets():
	"""Generate number of planets"""
	var namelen_rng = [3, 5]
	var nplanet_rng = [30, 60]
	var radius_rng = [500, 1500]

	# Set up variables
	var planet : Planet
	var planet_rad : float
	var planet_ang : float
	var planet_pos : Vector2

	# Generate new planet
	var nplanets = self.rng.randi_range(nplanet_rng[0], nplanet_rng[1])
	for ii in range(nplanets):
		# Instance
		planet = planet_scene.instantiate()
		planet.randomise(self.rng)
		# Name
		planet.name = NameGenerator.gen_name(namelen_rng, self.rng)
		while planet.name in self.solar_system.get_planet_properties('name'):
			planet.name = NameGenerator.gen_name(namelen_rng, self.rng)
		# Position
		while true:
			planet_rad = self.rng.randf_range(radius_rng[0], radius_rng[1])
			planet_ang = self.rng.randf_range(0, 2 * PI)
			planet_pos = Vector2(cos(planet_ang), sin(planet_ang)) * planet_rad
			var check = []
			for pos in self.solar_system.get_planet_properties('position'):
				check.append(planet_pos.distance_to(pos) < Planet.size_rng[1] * 30)
			if not true in check:
				break
		planet.position = planet_pos
		# Provide empire colours
		planet.colours = self.colours
		# Render
		planet.render()
		# Add to solar system
		self.solar_system.add_planet(planet)

func gen_system():
	"""Generate solar system connecting planets"""
	var nearest_planet : Planet
	var unconnected : Array
	var unlinked : Array

	# Link every planrt to closes unlinked planet
	for planet in self.solar_system.get_planets():
		# Get unlinked planets
		unlinked = self.solar_system.get_unlinked_planets(planet)
		while len(unlinked) > 0:
			# Find nearest planet
			nearest_planet = self.solar_system.get_nearest_planet(planet, unlinked)
			while nearest_planet in unlinked:
				unlinked.erase(nearest_planet)
			# Skip if creating intersecting links
			if self.solar_system.check_link_intersects(planet, nearest_planet):
				continue
			# Skip if link too close to other links
			if self.solar_system.check_link_angle(planet, nearest_planet):
				continue
			# Add link
			self.solar_system.add_link(planet, nearest_planet)
			break

	# Merge unlinked planet clusters
	for planet in self.solar_system.get_planets():
		# Find unconnected planets
		unconnected = self.solar_system.get_unconnected_planets(planet)
		if len(unconnected) == 0:
			break
		# Get nearest unconnected planet
		planet = self.solar_system.get_nearest_planet(planet, unconnected)
		# Find nearest unconnected planet of nearest planet
		unconnected = self.solar_system.get_unconnected_planets(planet)
		nearest_planet = self.solar_system.get_nearest_planet(planet, unconnected)
		self.solar_system.add_link(planet, nearest_planet)

	# Add more non-edge connections
	var non_edge_planets = self.solar_system.get_non_edge_planets()
	for planet in non_edge_planets:
		# Get unlinked planets
		unlinked = self.solar_system.get_unlinked_planets(planet, non_edge_planets)
		while len(unlinked) > 0:
			# Get nearest unlinked planet and remove from unlinked
			nearest_planet = self.solar_system.get_nearest_planet(planet, unlinked)
			while nearest_planet in unlinked:
				unlinked.erase(nearest_planet)
			# Add link if long path and not intersection
			var path = self.solar_system.find_path(planet, nearest_planet)
			# Skip planets too close
			if (len(path) - 1) < 1:
				continue
			# Skip if creating intersecting links
			if self.solar_system.check_link_intersects(planet, nearest_planet):
				continue
			# Skip if link too close to other links
			if self.solar_system.check_link_angle(planet, nearest_planet):
				continue
			# Create link
			self.solar_system.add_link(planet, nearest_planet)
			break

func gen_starting_locations():
	"""Generate starting locations for players"""

	# Check number of edges matches number of players
	var edge_planets = self.solar_system.get_edge_planets()
	if len(edge_planets) < len(self.players):
		assert('Not enough edge planets to support number of players')

	for player in self.players:
		var home_planet = edge_planets.pop_front()
		home_planet.populate(player)

# ---------------------------

func take_turn(task : String, options):
	"""Take turn and return bool to indicate if game has finished"""
	# Take action
	if task == 'colonise':
		# Options = [planet0_name: String]
		var planet0 = self.solar_system.get_planet(options)
		planet0.colonise(self.active_player)
		# print('Player %s colonised %s' % [active_player, planet0.name])
	elif task == 'fortify':
		# Options = [planet0_name: String]
		var planet0 = self.solar_system.get_planet(options)
		planet0.fortify(self.active_player)
		# print('Player %s fortified %s' % [active_player, planet0.name])
	elif task == 'move':
		# Options = [planet0_name: String, planet1_name: String, nforces: Int]
		var planet0 = self.solar_system.get_planet(options[0])
		var planet1 = self.solar_system.get_planet(options[1])
		var nforces = options[2]
		planet0.move(self.active_player, -nforces)
		planet1.move(self.active_player, nforces)
		# print('Player %s moved %s forces from %s to %s' % [active_player, nforces, planet0.name, planet1.name])
	# else:
		# Skip

	# Check players defeated
	var empires = self.solar_system.get_planet_properties('empire')
	for player in self.players:
		if not player in empires:
			self.players.erase(player)

	# Check only 1 player remaining
	if len(self.players) == 1:
		return true

	# Determine new active player
	var ind = self.players.find(self.active_player)
	ind = (ind + 1) % len(self.players)
	self.active_player = self.players[ind]

	# Reinforce
	if ind == 0:
		var planets = self.solar_system.get_planets()
		for planet in planets:
			planet.reinforce()
		self.round_num = self.round_num + 1

	# Announce new player
	emit_signal('turn_taken', [task, options])
	emit_signal('new_active_player', self.active_player)
	return false

# ---------------------------

func get_actions():
	"""Generate list of possible actions for active player"""
	var actions = [['skip', null]]

	var planets = solar_system.get_planets()
	for planet in planets:
		# Check empire
		if planet.empire != self.active_player:
			continue
		# Check forces available
		var forces = planet.forces
		if forces == 0:
			continue
		# Add colonise action
		var check = planet.check_can_colonise(active_player)
		if check:
			actions.append(['colonise', planet.name])
		# Add fortify action
		if planet.check_can_fortify(active_player):
			actions.append(['fortify', planet.name])
		# Add move action
		var links = planet.links
		for link in links:
			actions.append(['move', [planet.name, link.name, forces]])

	return actions

func take_action(action):
	"""MCTS wrapper for take_turn"""
	self.take_turn(action[0], action[1])

func score(player: String):
	"""Score current game state for given player"""
	var nplanets = len(self.solar_system.planets)
	var player_empire = 0
	var player_colony = 0
	var player_forces = 0
	var enemy_empire = 0
	var enemy_colony = 0
	var enemy_forces = 0

	var planets = self.solar_system.get_planets()
	for planet in planets:
		if planet.empire == player:
			player_empire += 1
			player_colony += planet.colony
			player_forces += planet.forces
		elif planet.empire != 'Neutral':
			enemy_empire += 1
			enemy_colony += planet.colony
			enemy_forces += planet.forces

	var score_player = player_forces + (player_colony * 10)
	var score_enemy = enemy_forces + (enemy_colony * 10)
	var score = float(score_player) / max(score_player + score_enemy, 1)
	return score
