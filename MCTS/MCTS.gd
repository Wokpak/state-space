class_name MCTS extends Node

# Algorithm parameters
var simulate_depth = 100
var min_evals = 500
var max_evals = 1000
const exp_factor = pow(2, 0.5)

# Node parameters
var parent : MCTS
var children : Array = []
var nvisits : int = 0
var nwins : float = 0

# Game parameters
var rootstate
var gamestate
var player: String
var active_player
var action

func _init(player: String, action, parent: MCTS = self):
	"""
	Create mcts node from action and current gamestate
	:param player: [String] Player
	:param action: [obj] Action object
	:param parent: [MCTS] Parent node (if root, parent set to self)
	"""
	self.name = 'MCTS'
	self.parent = parent
	self.action = action
	self.player = player

# -----------------

func search():
	"""
	Perform single MCTS loop and return action if max_evals reached
	:return: Best child object
	"""
	# Search if max evals not reached
	if self.nvisits < self.max_evals:
		var leaf = self.select(self)
		var child = self.expand(leaf)
		var score = self.simulate(child)
		self.backpropagate(child, score)

	# Max evals not reached
	if self.nvisits < self.min_evals:
		return

	# Not active player
	if not self.rootstate.active_player == self.player:
		return

	# Select action
	var action = self.make_choice()
	print('MCTS chosen action: %s (%s evals)' % [action, self.nvisits])
	for node in self.children:
		print('> %s | nvisits: %s | nwins: %s' % [node.action, node.nvisits, node.nwins])
	return action

func select(node: MCTS) -> MCTS:
	"""Select leaf node to expand"""
	# Duplicate rootstate
	node.gamestate = self.rootstate.clone()
	node.active_player = node.gamestate.active_player

	# Select leaf node
	while true:
		# Return unvisited
		if node.nvisits == 0:
			break
		# Return no children
		if len(node.children) == 0:
			break
		# Get new node
		node = node.get_uct_child()
		# Update gamestate
		node.update_gamestate()

	return node

func expand(node: MCTS) -> MCTS:
	"""Expand node and select child to simulate"""
	# Expand children
	var actions = node.gamestate.get_actions()
	for action in actions:
		var new_node = get_script().new(self.player, action, node)
		node.children.append(new_node)
		node.add_child(new_node)

	# No children
	if len(node.children) == 0:
		return node

	# Select child
	var child = node.children[randi() % len(node.children)]
	child.update_gamestate()
	return child

func simulate(node: MCTS) -> float:
	"""Simulate child node to max depth"""
	var actions = []
	var hasended = false

	# Simulate
	var depth = 0
	while depth < self.simulate_depth:
		actions = node.gamestate.get_actions()
		if len(actions) == 0:
			break
		actions = actions[randi() % len(actions)]
		hasended = node.gamestate.take_action(actions)
		if hasended:
			break
		depth = depth + 1

	# Get score
	var score = self.gamestate.score(self.player)

	# Delete temp gamestate
	self.gamestate.queue_free()

	return score

func backpropagate(node: MCTS, score):
	"""Backpropagate score to nodes in tree"""
	while true:
		node.nvisits += 1
		node.nwins += score
		# Exit if parent
		if node.parent == node:
			return
		node = node.parent

func make_choice():
	"""Get action visited most often"""
	var action
	var most_visits = 0
	var most_wins = 0
	for child in self.children:
		var wins = child.nwins
		if child.nvisits > most_visits:
			action = child.action
			most_visits = child.nvisits
		if child.nvisits == most_visits:
			if child.nwins >= most_wins:
				action = child.action
				most_wins = child.nwins

	return action

# -----------------

func get_uct_child():
	"""Return child with best upper confidence bound"""
	var leaf : MCTS
	var best_score = -INF
	for child in self.children:
		var score = child.calc_uct()
		if score >= best_score:
			best_score = score
			leaf = child
	return leaf

func calc_uct() -> float:
	"""Calculate upper confidence bound"""
	# Select unvisited
	if self.nvisits == 0:
		return INF

	# Determine if active player
	var exploitation = self.nwins / self.nvisits
	if not self.parent.active_player == self.player:
		exploitation = -exploitation
	var exploration = self.exp_factor * pow(log(self.parent.nvisits) / self.nvisits, 0.5)
	var score = exploitation + exploration
	return score

func update_gamestate():
	"""Update gamestate using parents state and action"""
	self.gamestate = self.parent.gamestate
	self.gamestate.take_action(self.action)
	self.active_player = self.gamestate.active_player

func update_root(action) -> MCTS:
	"""Find gamestate after action taken from current rootstate"""
	# Find matching child
	for child in self.children:
		if child.action == action:
			child.parent = child
			child.rootstate = self.rootstate
			return child

	# Reset tree as no matching child found
	print('MCTS: No matching root state found, resetting tree')
	self.nvisits = 0
	self.nwins = 0
	# BUG HERE: NOT ALL CHILDREN ARE REMOVED, LIKELY MAIN ISSUE
	for child in self.children:
		#self.children.erase(child)
		child.queue_free()
	self.children = [];
	return self
