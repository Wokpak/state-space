class_name PlanetOld extends Node2D

const planet_render = preload("res://Planet_render.tscn")

# Planet properties
var size : int # Size of planet
var type : String # Type of planet
var atmos : float #  Available atmophere on planet
var matter : float # Available material on planet
var energy : float # Available energy on planet

# Empire properties
var empire : String = 'Neutral' # Occupying empire
var forces : int = 0 # Number of occupying forces
var colony : int = 0 # Number of colonies established
var defences : int = 0 # Number of defensive installations

# Other
var links : Array = [] # Linked planets
var colours : Dictionary
var rend : Planet_render
var rng : RandomNumberGenerator

# Game Settings
const cost_fortify_base : int = 5
const cost_fortify_factor : int = 2
const cost_colonise_base : int = 5
const cost_colonise_factor : int = 2

# Property ranges
const namelen_rng = [4, 6]
const type_opt = ['Terra', 'Oceanic', 'Icy', 'Gasious']
const size_rng = [2, 5]

# Signals
signal selected

# BUILT-INS ---------------

func _ready():
	pass

# METHODS -----------------

func clone():
	"""Create clone of planet"""
	var clone = get_script().new()
	clone.name = self.name
	var props = self.get_script().get_script_property_list()
	for prop in props:
		if not prop.name == 'rend':
			clone.set(prop.name, self.get(prop.name))
	return clone

func randomise(rng: RandomNumberGenerator):
	"""Randomise planet"""
	self.rng = rng
	# Generate planet properties
	self.size = self.rng.randi_range(size_rng[0], size_rng[1])
	self.type = self.type_opt[self.rng.randi_range(0, self.type_opt.size() - 1)]
	self.atmos = round(self.rng.randf() * 100) / 100
	self.matter = round(self.rng.randf() * 100) / 100
	self.energy = round(self.rng.randf() * 100) / 100

func render():
	"""Add planet sprite and info"""
	rend = planet_render.instantiate()
	self.add_child(rend)
	rend.initialise()

func link_client(client):
	self.rend.link_client(client)

func update():
	"""Update render"""
	if not self.get('rend') == null:
		rend.update_values()

# ACTIONS ---------------

func populate(player : String):
	"""Set up planet for player"""
	self.empire = player
	self.colony = 1
	self.forces = 5

	# Update render
	if not self.get('rend') == null:
		rend.update_values()

func move(player : String, moved_forces : int):
	"""Modify number of troops on planet"""
	# Friendly/Uninhabited
	if self.empire in ['Neutral', player]:
		# Set troop number
		self.forces = self.forces + moved_forces
		# Colonise
		if self.empire == 'Neutral':
			self.empire = player
		# Abandon
		if (self.forces + self.colony) == 0:
			self.empire = 'Neutral'
		# Friendly
		self.update()
		return

	# Hostile
	var roll_attack = 0
	var roll_defend = 0
	while self.forces > 0 and moved_forces > 0:
		# Roll
		roll_attack = self.rng.randi_range(1, moved_forces) / (self.defences + 1)
		roll_defend = self.rng.randi_range(1, self.forces)
		# Modify
		self.forces = max(0, self.forces - roll_attack)
		moved_forces = max(0, moved_forces - roll_defend)

	# Check who lost first
	if self.forces + moved_forces == 0:
		self.forces = int(roll_defend > roll_attack)
		moved_forces = int(roll_defend > roll_attack)

	# Neutral
	if moved_forces > 0:
		self.empire = player
		self.forces = moved_forces
		self.colony = 0
		self.defences = 0

	# Update render
	self.update()

func colonise(player : String):
	"""Colonise planet"""
	# Check player controls planet
	if player != self.empire:
		push_error('Player %s does not control planet %s' % [player, self.name])
		return
	# Check player has enough forces
	var cost = self.get_colonise_cost()
	if self.forces < cost:
		push_error('Player %s does not have enough forces to colonise planet %s' % [player, self.name])
		return
	# Fortify
	self.forces = self.forces - cost
	self.colony = self.colony + 1

	# Update render
	self.update()

func fortify(player : String):
	"""Fortify planet"""
	# Check player controls planet
	if player != self.empire:
		push_error('Player %s does not control planet %s' % [player, self.name])
		return
	# Check player has enough forces
	var cost = self.get_fortify_cost()
	if self.forces < cost:
		push_error('Player %s does not have enough forces to fortfiy planet %s' % [player, self.name])
		return
	# Fortify
	self.forces = self.forces - cost
	self.defences = self.defences + 1

	# Update render
	self.update()

func reinforce():
	"""Increase forces by amount depending on colony size"""
	self.forces = self.forces + self.colony

	# Update render
	self.update()

func get_colonise_cost() -> int:
	"""Calculate cost to colonise planet"""
	return int(cost_colonise_base * pow(cost_colonise_factor, self.colony))

func get_fortify_cost() -> int:
	"""Calculate cost to fortify planet"""
	return int(cost_fortify_base * pow(cost_fortify_factor, self.defences))

func check_can_colonise(player : String) -> bool:
	"""Check if player can colonise planet"""
	# Check player controls planet
	if player != self.empire:
		return false
	# Check player has enough forces
	var cost = self.get_colonise_cost()
	if self.forces < cost:
		return false
	return true

func check_can_fortify(player : String) -> bool:
	"""Check if player can fortify planet"""
	# Check player controls planet
	if player != self.empire:
		return false
	# Check player has enough forces
	var cost = self.get_fortify_cost()
	if self.forces < cost:
		return false
	return true
