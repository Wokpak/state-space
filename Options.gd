extends VBoxContainer

# Define variables
var planet : Planet
var panel = null

# Define signals
signal chosen_task

func _ready():
	pass

func initialise(client, selected_planet: Planet):
	"""Set up options"""
	# Add options panel to planet
	self.planet = selected_planet
	panel = self.planet.get_node('planet_render/Panel/Info/HBox')
	panel.add_child(self)

	# Link signals to client
	self.connect('chosen_task',Callable(client,'_on_task_selected'))

	# Hide suboptions
	$main_options.visible = true
	$move_options.visible = false

	# Set costs and disable invalid options
	var cost_colonise = planet.get_colonise_cost()
	$main_options/buttom_colonise.text = 'COLONISE: %s' % cost_colonise
	if self.planet.forces < cost_colonise:
		$main_options/buttom_colonise.disabled = true
	var cost_fortify = planet.get_fortify_cost()
	$main_options/buttom_fortify.text = 'FORTIFY: %s' % cost_fortify
	if self.planet.forces < cost_fortify:
		$main_options/buttom_fortify.disabled = true

func delete():
	# Delete options from planet
	if panel != null:
		panel.remove_child(self)

func _on_buttom_move_pressed():
	# Emit signal
	emit_signal('chosen_task', 'move')
	# Show suboptions
	$main_options.visible = false
	$move_options.visible = true
	# Set up slider and text
	$move_options/slider.min_value = 1
	$move_options/slider.max_value = self.planet.forces
	$move_options/slider.value = $move_options/slider.max_value

func _on_buttom_colonise_pressed():
	emit_signal('chosen_task', 'colonise')

func _on_buttom_fortify_pressed():
	emit_signal('chosen_task', 'fortify')

func _on_slider_value_changed(value):
	$move_options/text_input.text = str(value)

func _on_text_input_text_changed(new_text):
	# Check only numbers entered
	for letter in new_text:
		if int(letter) == 0 and letter != '0':
			$move_options/text_input.text = str($move_options/slider.value)
			return

	# Check value within correct range
	var value = int($move_options/text_input.text)
	if value < $move_options/slider.min_value:
		$move_options/slider.value = $move_options/slider.min_value
		$move_options/text_input.text = str($move_options/slider.min_value)
		return
	if value > $move_options/slider.max_value:
		$move_options/slider.value = $move_options/slider.max_value
		$move_options/text_input.text = str($move_options/slider.max_value)
		return

	# Set value
	$move_options/slider.value = int($move_options/text_input.text)

#func _on_button_confirm_pressed():
#	var nforces = int($move_options/slider.value)
#	emit_signal('chosen_task', 'confirm', nforces)
