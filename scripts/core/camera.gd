extends Camera2D

var interactive = true
var move_speed = 1000
var zoom_speed = 0.05
var zoom_min = 0.5

func _ready():
	pass

func _process(delta):
	# Check interactive
	if not interactive:
		return

	# Pan camera
	self.pan_camera(delta)
	
	# Set background material position
	$Background/Sphere.material.set_shader_parameter('sphere_position', self.get_screen_center_position())
	$Background/Sphere.material.set_shader_parameter('screen_position',  -self.get_viewport().get_visible_rect().size / 2)
	$Background/Stars.material.set_shader_parameter('offset', self.get_screen_center_position())

func pan_camera(delta: float):
	var input_vector = Vector2()

	if Input.is_action_pressed("ui_up"):
		input_vector.y -= 1
	if Input.is_action_pressed("ui_down"):
		input_vector.y += 1
	if Input.is_action_pressed("ui_left"):
		input_vector.x -= 1
	if Input.is_action_pressed("ui_right"):
		input_vector.x += 1

	# Normalize the input vector to avoid faster diagonal movement
	if input_vector.length() > 0:
		input_vector = input_vector.normalized()
		
	# Move the camera
	self.position += input_vector * move_speed * delta

func _input(event):
	# Check interactive
	if not interactive:
		return
	if event.is_action_released('scroll_up'):
		self.zoom_camera(-zoom_speed, event.position)
	if event.is_action_released('scroll_down'):
		self.zoom_camera(zoom_speed, event.position)

func zoom_camera(zoom_factor, mouse_position):
	# Calculate zoom level
	var viewport_size = self.get_viewport_rect().size
	var previous_zoom = self.zoom
	var new_zoom = self.zoom + self.zoom * zoom_factor
	var limits = Vector2(self.limit_right - self.limit_left, self.limit_bottom - self.limit_top)
	# Ensure zoom keeps game within camera limits
	if (viewport_size * new_zoom).x > self.limit_right - self.limit_left:
		return
	if (viewport_size * new_zoom).y > self.limit_bottom - self.limit_top:
		return
	# Ensure zoom exceeds minimum levels
	if new_zoom < Vector2(1, 1) * self.zoom_min:
		return
	# Apply zoom/position
	self.zoom += self.zoom * zoom_factor
	self.position += ((viewport_size * 0.5) - mouse_position) * (self.zoom - previous_zoom)
