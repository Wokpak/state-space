extends GutTest

func test_planet():
	# Create planet
	var planet = Planet.new('test')
	
	# Test inheritance has worked correctly
	assert_true(planet.id == 'test')
	assert_true(planet.edges == {})
	
	# Modify planet specific parameters
	planet.size = 5
	assert_true(planet.size == 5)

func test_planet_distance_to():
	# Create planets
	var planet = Planet.new('planet')
	var target = Planet.new('target')
	var dist: float

	planet.position = Vector2(0, 0)
	target.position = Vector2(0, 10)
	assert_true(planet.distance_to(target) == 10)

	planet.position = Vector2(-10, 0)
	target.position = Vector2(10, 0)
	assert_true(planet.distance_to(target) == 20)

	planet.position = Vector2(0, 10)
	target.position = Vector2(0, 10)
	assert_true(planet.distance_to(target) == 0)

	planet.position = Vector2(0, 0)
	target.position = Vector2(3, 4)
	assert_true(planet.distance_to(target) == 5)
