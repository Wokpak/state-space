extends GutTest

func test_gen_planets():
	# Create engine
	var engine = GameEngine.new(0)
	
	# Generate planets
	engine.gen_planets(10)
	
	# Check planets added to graph
	# Check planets all have unique names
	assert_true(engine.vertices.size() == 10)
	
	# Check planets are uniformly spaced
	# TODO

func test_get_nearest_planet():
	# Create engine
	var engine = GameEngine.new(0)
	
	# Create planets
	var planet0 = Planet.new('p0')
	var planet1 = Planet.new('p1')
	var planet2 = Planet.new('p2')
	var planet3 = Planet.new('p3')
	var planet4 = Planet.new('p4')
	
	# Position planets
	planet0.position = Vector2(0, 0)
	planet1.position = Vector2(0, 1000)
	planet2.position = Vector2(100, 100)
	planet3.position = Vector2(100, 0)
	planet4.position = Vector2(0, 100)
	
	# Add to engine
	engine.add_vertex(planet0)
	engine.add_vertex(planet1)
	engine.add_vertex(planet2)
	engine.add_vertex(planet3)
	engine.add_vertex(planet4)

	# Check removal of self as valid option
	var nearest = engine.get_nearest_planet(planet0, [planet0, planet1, planet2, planet3, planet4])
	assert_false(nearest == planet0)
	assert_true(nearest == planet3)

	# Get nearest as nearest planet
	nearest = engine.get_nearest_planet(planet0, [planet1, planet2, planet3])
	assert_true(nearest == planet3)
	
	# Get first nearest as nearest
	nearest = engine.get_nearest_planet(planet0, [planet1, planet2, planet3, planet4])
	assert_true(nearest == planet3)

func test_check_edge_intersects():
	# Create engine
	var engine = GameEngine.new(0)
	
	# Create planets
	var planet0 = Planet.new('p0')
	var planet1 = Planet.new('p1')
	var planet2 = Planet.new('p2')
	var planet3 = Planet.new('p3')
	var planet4 = Planet.new('p4')
	var planet5 = Planet.new('p5')
	
	# Position planets
	planet0.position = Vector2(0, 0)
	planet1.position = Vector2(3, 3)
	planet2.position = Vector2(0, 3)
	planet3.position = Vector2(3, 0)
	planet4.position = Vector2(-2, 2)
	planet5.position = Vector2(2, -2)
	
	# Add to engine
	engine.add_vertex(planet0)
	engine.add_vertex(planet1)
	engine.add_vertex(planet2)
	engine.add_vertex(planet3)
	engine.add_vertex(planet4)
	engine.add_vertex(planet5)
	
	# Check
	engine.add_edge('p0', 'p1')
	var check = engine.check_edge_intersects(planet0, planet2)
	assert_false(check)
	check = engine.check_edge_intersects(planet0, planet3)
	assert_false(check)
	check = engine.check_edge_intersects(planet2, planet3)
	assert_true(check)
	check = engine.check_edge_intersects(planet4, planet5)
	assert_true(check)
	check = engine.check_edge_intersects(planet3, planet5)
	assert_false(check)
	check = engine.check_edge_intersects(planet3, planet4)
	assert_true(check)

func test_get_player_planets():
	var engine = GameEngine.new(0)
	
	# Create planets
	var planet0 = Planet.new('p0')
	var planet1 = Planet.new('p1')
	var planet2 = Planet.new('p2')
	var planet3 = Planet.new('p3')
	var planet4 = Planet.new('p4')
	var planet5 = Planet.new('p5')
	
	# Set players
	planet0.empire = 'p1'
	planet1.empire = 'p1'
	planet3.empire = 'p3'
	planet5.empire = 'p5'
	
	# Add to engine
	engine.add_vertex(planet0)
	engine.add_vertex(planet1)
	engine.add_vertex(planet2)
	engine.add_vertex(planet3)
	engine.add_vertex(planet4)
	engine.add_vertex(planet5)
	
	# Check conquered_planets
	var conquered_planets = engine.get_conquered_planets()
	assert_true(len(conquered_planets) == 4)
	assert_true(planet0 in conquered_planets)
	assert_true(planet1 in conquered_planets)
	assert_true(planet2 not in conquered_planets)
	assert_true(planet3 in conquered_planets)
	assert_true(planet4 not in conquered_planets)
	assert_true(planet5 in conquered_planets)
	
		# Check conquered_planets
	var neutral_planets = engine.get_neutral_planets()
	assert_true(len(neutral_planets) == 2)
	assert_true(planet0 not in neutral_planets)
	assert_true(planet1 not in neutral_planets)
	assert_true(planet2 in neutral_planets)
	assert_true(planet3 not in neutral_planets)
	assert_true(planet4 in neutral_planets)
	assert_true(planet5 not in neutral_planets)
