extends GutTest

func test_game_render():
	# Set up game engine
	var engine = GameEngine.new()
	engine.gen_planets(10)
	
	# Render
	var render = GameRender.new(engine)
	render.render()
	
	# Close render
	render.close()
