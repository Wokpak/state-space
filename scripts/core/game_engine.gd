class_name GameEngine extends Graph
"""
Game engine (representing a graph)
"""

# Define variables
var players : Array
var active_player : String
var round_num : int
var rng : RandomNumberGenerator

# Define signals
signal new_active_player

func _init(game_seed : int = 0) -> void:
	# Set up random number generator for world generation
	self.rng = RandomNumberGenerator.new()
	self.rng.set_seed(game_seed)

## Start new game
func new_game(players : Array, game_seed : int):
	# Set player info
	self.players = players
	self.active_player = self.players[0]
	
	# Initialise random number generator for game
	self.rng = RandomNumberGenerator.new()
	self.rng.set_seed(game_seed)
	
	# Generate world
	self.gen_starting_locations()
	self.gen_planets(20)
	self.gen_links()
	
	# Initialise random number generator for game
	self.rng.seed = randi()
	
	# Link 

	# Begin game
	self.round_num = 0
	emit_signal('new_active_player', self.active_player)

# World generation ------------------------------

func gen_starting_locations():
	"""Generate starting location for players"""
	# Get number of players
	var nplayers = len(self.players)

	# Get relevant settings
	var namelen_min = Config.get_value("planet", "namelen_min")
	var namelen_max = Config.get_value("planet", "namelen_max")
	var radius_max = Config.get_value("planet", "orbit_radius_max")
	var size_max = Config.get_value("planet", "size_max")
	var starting_forces = Config.get_value("game", "starting_forces")
	var starting_colony = Config.get_value("game", "starting_colony")

	# Create planets
	for ii in range(nplayers):
		# Create unique planet name
		var name = NameGenerator.gen_name_randlen(namelen_min, namelen_max, self.rng)
		while name in self.vertices.keys():
			name = NameGenerator.gen_name_randlen(namelen_min , namelen_max, self.rng)
		# Generate planet and add it to the graph
		var new_planet = Planet.new(name)
		self.add_vertex(new_planet)
		# Set position and size
		var angle = PI + ((2 * PI) / nplayers) * ii
		new_planet.position = Vector2(cos(angle), sin(angle)) * radius_max
		new_planet.size = size_max
		# Set empire and starting colony/forces
		new_planet.empire = self.players[ii]
		new_planet.forces = starting_forces
		new_planet.colony = starting_colony

func gen_planets(nplanets: int):
	"""Generate a nplanets and position them withinsolar system
	
	:param nplanets: [int] Number of planets to generate
	"""
	# Set up variables
	var name : String
	var new_planet : Planet
	var radius : float
	var angle : float
	var position : Vector2
	
	# Get relevant settings
	var namelen_min = Config.get_value("planet", "namelen_min")
	var namelen_max = Config.get_value("planet", "namelen_max")
	var radius_min = Config.get_value("planet", "orbit_radius_min")
	var radius_max = Config.get_value("planet", "orbit_radius_max")
	var size_min = Config.get_value("planet", "size_min")
	var size_max = Config.get_value("planet", "size_max")
	var planet_spacing = Config.get_value("planet", "planet_spacing")

	# Create planets
	for ii in range(nplanets):
		# Create unique planet name
		name = NameGenerator.gen_name_randlen(namelen_min, namelen_max, self.rng)
		while name in self.vertices.keys():
			name = NameGenerator.gen_name_randlen(namelen_min , namelen_max, self.rng)
		# Generate planet with unique id and add it to the graph
		new_planet = Planet.new(name)
		self.add_vertex(new_planet)
		# Generate valid position
		while true:
			# Generate random position in polar coordinates
			radius = self.rng.randf_range(radius_min, radius_max)
			angle = self.rng.randf_range(0, 2 * PI)
			# Convert to cartesian coordinates
			position = Vector2(cos(angle), sin(angle)) * radius
			new_planet.position = position
			# Check spacing
			var nearest = self.get_nearest_planet(new_planet, self.vertices.values())
			if nearest == null:
				break
			if new_planet.distance_to(nearest) >= planet_spacing:
				break
		# Set other properties
		new_planet.size = self.rng.randi_range(size_min, size_max)

func gen_links():
	"""Generate links between planets"""
	var radius_min = Config.get_value("planet", "orbit_radius_min") * 0.9

	# Link starting planets to closest planets
	var conquered_planets = self.get_conquered_planets()
	for planet in conquered_planets:
		# Find nearest planet
		var nearest_planet = self.get_nearest_planet(planet)
		# Add link
		self.add_edge(planet.id, nearest_planet.id)

	# Link every planet to closes isolated planet
	for planet in self.get_neutral_planets():
		# Get unreachable neutral planets
		var unreachable = self.get_unreachable_vertices(planet)
		unreachable = self.get_neutral_planets(unreachable)
		while len(unreachable) > 0:
			# Find nearest planet
			var nearest_planet = self.get_nearest_planet(planet, unreachable)
			# Erase from list
			unreachable.erase(nearest_planet)
			# Skip if creating intersecting links
			if self.check_edge_intersects(planet, nearest_planet):
				continue
			# Skip if edge too close to other links
			if self.check_edge_angle(planet, nearest_planet):
				continue
			# Skip if edge too close to minimum radius
			if self.check_line_clearance(planet, nearest_planet, Vector2(0, 0), radius_min):
				continue
			# Add edge
			self.add_edge(planet.id, nearest_planet.id)
			break

	# Merge unlinked planet clusters
	for planet in self.get_neutral_planets():
		# Find unreachable neutral planets
		var unreachable = self.get_unreachable_vertices(planet)
		unreachable = self.get_neutral_planets(unreachable)
		if len(unreachable) == 0:
			break
		# Get nearest unreachable plane
		planet = self.get_nearest_planet(planet, unreachable)
		# Find nearest unconnected planet of nearest planet
		unreachable = self.get_unreachable_vertices(planet)
		var nearest_planet = self.get_nearest_planet(planet, unreachable)
		# Skip if edge too close to minimum radius
		if self.check_line_clearance(planet, nearest_planet, Vector2(0, 0), radius_min):
			continue
		# Add edge
		self.add_edge(planet.id, nearest_planet.id)

	# Add more branch connections
	var branch_planets = self.get_branch_vertices()
	for planet in branch_planets:
		# Get non-edge branches of planet
		var non_edges = self.get_non_edge_vertices(planet, branch_planets)
		while len(non_edges) > 0:
			# Get nearest non-edge planet and remove from non_edges
			var nearest_planet = self.get_nearest_planet(planet, non_edges)
			non_edges.erase(nearest_planet)
			# Add edge if long path and not intersecting
			var path = self.find_path(planet, nearest_planet)
			# Skip planets too close
			if (len(path) - 1) < 1:
				continue
			# Skip if creating intersecting links
			if self.check_edge_intersects(planet, nearest_planet):
				continue
			# Skip if edge too close to other links
			if self.check_edge_angle(planet, nearest_planet):
				continue
			# Skip if edge too close to minimum radius
			if self.check_line_clearance(planet, nearest_planet, Vector2(0, 0), radius_min):
				continue
			# Create edge
			self.add_edge(planet.id, nearest_planet.id)
			#break

# Graph functions -------------------------------

func get_neutral_planets(planets : Array = self.vertices.values()) -> Array:
	"""Get list of neutral planets (not conquered by any player)"""
	planets = planets.filter(func(planet): return planet.empire == 'Neutral')
	return planets

func get_conquered_planets(planets : Array = self.vertices.values()) -> Array:
	"""Get list of planets conquered by any player"""
	planets = planets.filter(func(planet): return planet.empire != 'Neutral')
	return planets

func get_nearest_planet(planet: Planet, targets: Array = self.vertices.values()) -> Planet:
	"""Return nearest target planet to planet"""
	# Check list of targets provided
	assert(len(targets) > 0)
	
	# Remove self from options
	targets.erase(planet)
	
	var mindist : float = INF
	var nearest : Planet = null

	for target in targets:
		var dist = planet.distance_to(target)
		if dist < mindist:
			mindist = dist
			nearest = target

	return nearest

func check_edge_intersects(planet0: Planet, planet1: Planet) -> bool:
	"""Check if edge between planet and target intersects another link"""
	# Check planet and target are different
	assert(not planet0 == planet1)

	# Get proposed edge points
	var line0_pt0 = planet0.position
	var line0_pt1 = planet1.position

	# Get all existing edges
	var edges = self.get_all_edges()

	# Check each edge
	for edge in edges:
		# Skip edges starting/ending at planet or target
		if planet0 in edge or planet1 in edge:
			continue
		# Get edge end points
		var line1_pt0 = edge[0].position
		var line1_pt1 = edge[1].position
		# Check for intersect
		var check = Geometry2D.segment_intersects_segment(line0_pt0, line0_pt1, line1_pt0, line1_pt1)
		if check is Vector2:
			return true

	return false

func check_edge_angle(planet0 : Planet, planet1 : Planet) -> bool:
	"""Check if edge between given planets is too close in angle to existing links"""
	# Check planets different
	assert(not planet0 == planet1)

	# Define minimum angle
	var angmin = Config.get_value("planet", "planet_link_angle_min")
	
	# Check planet0
	var ang0 = planet0.position.angle_to_point(planet1.position)
	var edges0 = planet0.edges.values()
	for edge in edges0:
		var ang = planet0.position.angle_to_point(edge.position)
		var dang = rad_to_deg(abs(ang0 - ang))
		if min(dang, 360 - dang) < angmin:
			return true

	# Check planet1
	var ang1 = planet1.position.angle_to_point(planet0.position)
	var edges1 = planet1.edges.values()
	for edge in edges1:
		var ang = planet1.position.angle_to_point(edge.position)
		var dang = rad_to_deg(abs(ang1 - ang))
		if min(dang, 360 - dang) < angmin:
			return true

	return false

func check_line_clearance(planet0 : Planet, planet1 : Planet, origin: Vector2, distance : float):
	"""Check that line does not go withing distance of specific point"""
	# Get positions of the planets
	var point0 = planet0.position
	var point1 = planet1.position

	# Get the closest point on the segment to the origin
	var closest_point = Geometry2D.get_closest_point_to_segment(origin, point0, point1)

	# Check if the distance from the origin to the closest point is within the threshold
	return closest_point.distance_to(origin) <= distance
