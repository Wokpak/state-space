extends Node2D

# Import classes
const main_menu_scene = preload("res://scenes/MenuMain.tscn")
const gui_scene = preload("res://scenes/UserInterface.tscn")
#const engine_scene = preload("res://GameEngine.tscn")
const options_scene = preload("res://Options.tscn")

# Define variables
var main_menu
var gui : Gui
var engine : GameEngine
var render : GameRender
var options = null
var controlled_players : Array
var player_name : String
var toggle_move : bool = false
var selected_planet0 : Planet = null
var selected_planet1 : Planet = null

func _ready():
	# Set up randomisation
	randomize()
	# Instance and launch main menu
	main_menu = main_menu_scene.instantiate()
	self.add_child(main_menu)
	main_menu.initialise(self)
	# Play animation
	$Camera/AnimationPlayer.play('on_launch')

func _input(event):
	# Reset user inputs on escape
	if event.is_action_pressed("ui_cancel"):
		self.selected_planet0 = null
		self.selected_planet1 = null
		self.toggle_move = false
		if self.options != null:
			self.options.queue_free()
			self.options = null

func _on_start_new_game(game_seed, player_names, player_iscomputer):
	# Close main menu
	main_menu.queue_free()

	# Start gui
	self.gui = gui_scene.instantiate()
	self.add_child(gui)

	# Set up game engine
	self.engine = GameEngine.new()
	self.engine.new_game(player_names)
	
	# Set up renderer
	self.render = GameRender.new(self.engine)
	self.add_child(self.render)
	self.render.render()

	# -------------------

	# Instance game engine and launch game
	#self.engine = engine_scene.instantiate()
	#self.add_child(self.engine)

	#self.engine.new_game(player_names, player_colours, game_seed, player_iscomputer)

	# Record active player and those controlled by client
	self.controlled_players = player_names
	self.player_name = player_names[0]

	# Enable camera movements
	$Camera3D.interactive = true

	# Transition star to game size
	$Background/AnimationPlayer.play("OnStartGame")

func _on_new_active_player(player_name):
	"""Reset user controls on new players turn"""
	# Update gui
	self.gui.on_new_active_player(player_name)

	# Reset user inputs
	self.selected_planet0 = null
	self.selected_planet1 = null
	self.toggle_move = false
	if self.options != null:
		self.options.queue_free()
		self.options = null

	# Check if client controlls this player
	if player_name in self.controlled_players:
		self.player_name = player_name

func _on_planet_selected(planet : Planet):
	# Not activie player
	if self.engine.active_player != self.player_name:
		return

	# No planet selected
	if planet == null:
		# Remove existing options
		if options != null:
			options.queue_free()
			options = null
		return

	# Select first planet
	if self.selected_planet0 == null:
		# Not players planet
		if planet.empire != self.player_name:
			return
		# Record selection
		self.selected_planet0 = planet
		# Display options
		options = options_scene.instantiate()
		options.initialise(self, planet)
		return

	# Select first planet selected
	if self.selected_planet0 == planet:
		return

	# New first planet selected
	if toggle_move == false:
		# Remove old options
		options.queue_free()
		options = null
		# Not players planet
		if planet.empire != self.player_name:
			self.selected_planet0 = null
			return
		# Record selection
		self.selected_planet0 = planet
		# Display new options
		options = options_scene.instantiate()
		options.initialise(self, planet)
		return

	# Check second planet connected to first
	if not planet in self.selected_planet0.links:
		self.selected_planet1 = null
		return

	# Select new second planet
	self.selected_planet1 = planet
	var nforces = int(options.get_node('move_options/slider').value)
	self.engine.take_turn('move', [self.selected_planet0.name, self.selected_planet1.name, nforces])

func _on_task_selected(task : String, nforces : int = 0):
	# Colonise/fortify
	if task in ['colonise', 'fortify']:
		self.engine.take_turn(task, self.selected_planet0.name)

	# Move
	if task == 'move':
		self.toggle_move = true
