class_name Planet extends GraphVertex
"""
Planet class (representing a graph vertex)
"""

# Planet properties
var position : Vector2 # Cartesian position of planet
var size : int # Size of planet

# Empire properties
var empire : String = 'Neutral' # Occupying empire
var forces : int = 0 # Number of occupying forces
var colony : int = 0 # Number of colonies established
var defences : int = 0 # Number of defensive installations

func distance_to(planet: Planet) -> float:
	"""Calculate distance to target planet
	
	:param planet: [Planet] Target planet
	:returns: [float] Distance to target planet
	"""
	# Todo: check target planet has its position set
	var distance : float = self.position.distance_to(planet.position)
	return distance
