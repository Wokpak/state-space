extends CanvasLayer

const player_scene = preload("res://Player.tscn")
@export var player_colors = [
	Color('1D2B53'),
	Color('7E2553'),
	Color('008751'),
	Color('AB5236'),
	Color('5F574F'),
	Color('C2C3C7'),
	Color('FF004D'),
	Color('FFA300'),
	]

var game_launched = 0;

signal start_new_game

func _ready():
	pass

func initialise(parent):
	"""Initiaise menu"""
	# Link signals
	self.connect('start_new_game',Callable(parent,'_on_start_new_game'))
	# Enable
	self._enable()

func _enable():
	"""Reset options and toggle visability"""
	# Reset menu visibility
	$container.visible = true
	$container/start.visible = true
	$container/new_game.visible = false
	$container/start/button_new.grab_focus()
	if game_launched == 0:
		$AnimationPlayer.play("OnLaunch")
		game_launched = 1;

func _disable():
	"""Toggle visability"""
	# Hide menu
	$container.visible = false

func _input(event):
	# Reset user inputs on escape
	if event.is_action_pressed("ui_cancel"):
		self._enable()

func _on_button_new_pressed():
	# Switch menus
	$container/start.visible = false
	$container/new_game.visible = true

	# Remove existing player info
	var player_info = $container/new_game/player_container/player_info
	for player in player_info.get_children():
		player_info.remove_child(player)

	# Randomise seed
	var random_seed = str(randi() % int(1e3))
	$container/new_game/text_seed.placeholder_text = random_seed

	# Add player info
	self._on_button_add_player_pressed()
	self._on_button_add_player_pressed()

func _on_button_exit_pressed():
	# Exit game
	get_tree().quit()

func _on_button_start_pressed():
	var player_names = []
	var player_colours = []
	var player_iscomputer = []
	var name : String
	var color : Color
	var iscomputer : bool

	# Get seed
	var game_seed = $container/new_game/text_seed.text
	if len(game_seed) == 0:
		game_seed = $container/new_game/text_seed.placeholder_text
	game_seed = int(game_seed)
	# Get player info
	var player_info = $container/new_game/player_container/player_info
	for player in player_info.get_children():
		# Get name
		name = player.get_node('Vbox/player_name').text
		if len(name) == 0:
			name = player.get_node('Vbox/player_name').placeholder_text
		player_names.append(name)
		# Get colour
		color = player.get_node('Vbox/player_colour').color
		player_colours.append(color)
		# Get iscomputer
		iscomputer = player.get_node('Vbox/player_iscomputer').button_pressed
		player_iscomputer.append(iscomputer)

	# Send new game command to client
	emit_signal('start_new_game', game_seed, player_names, player_iscomputer)

func _on_button_add_player_pressed():
	# Get number of players
	var nplayers = $container/new_game/player_container/player_info.get_child_count()

	# Add additional player
	var player = player_scene.instantiate()
	player.name = 'Player ' + str(nplayers + 1)
	player.get_node("Vbox/player_name").placeholder_text = 'PLAYER ' + str(nplayers + 1)
	player.get_node("Vbox/player_colour").color = self.player_colors[nplayers]
	$container/new_game/player_container/player_info.add_child(player)

	# Set add/remove button visbility
	if nplayers + 1 <= 2:
		$container/new_game/player_container/buttons/button_add_player.disabled = false
		$container/new_game/player_container/buttons/button_remove_player.disabled = true
		return
	if nplayers + 1 >= 8:
		$container/new_game/player_container/buttons/button_add_player.disabled = true
		$container/new_game/player_container/buttons/button_remove_player.disabled = false
		return
	$container/new_game/player_container/buttons/button_add_player.disabled = false
	$container/new_game/player_container/buttons/button_remove_player.disabled = false

func _on_button_remove_player_pressed():
	var player = $container/new_game/player_container/player_info.get_children()[-1]
	$container/new_game/player_container/player_info.remove_child(player)

	# Set add/remove button visbility
	var nplayers = $container/new_game/player_container/player_info.get_child_count()
	if nplayers <= 2:
		$container/new_game/player_container/buttons/button_add_player.disabled = false
		$container/new_game/player_container/buttons/button_remove_player.disabled = true
		return
	if nplayers >= 8:
		$container/new_game/player_container/buttons/button_add_player.disabled = true
		$container/new_game/player_container/buttons/button_remove_player.disabled = false
		return
	$container/new_game/player_container/buttons/button_add_player.disabled = false
	$container/new_game/player_container/buttons/button_remove_player.disabled = false
