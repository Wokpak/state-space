class_name LinkRender extends Node2D

## Planets forming link
var planets : Array

var points = [Vector2(0, 0), Vector2(0, 0)]
var height = 3

func _ready():
	pass # Replace with function body.

func initialise(planet0: Planet, planet1: Planet):
	# Link planet objects
	self.planets = [planet0, planet1]
	
	# Set new points
	self.points = [self.planets[0].position, self.planets[1].position]

	# Calculate size, position and angle
	var dif = self.points[1] - self.points[0]
	var width = pow(pow(dif[0], 2) + pow(dif[1], 2), 0.5)

	# Set rect position
	$Rect.size = Vector2(width, height)
	$Rect.position = -Vector2(width, height) / 2

	# Set node position
	self.position = self.points[0] + (dif / 2)
	self.rotation = atan(dif[1] / dif[0])

#func _input(event):
#	if event is InputEventMouseButton and event.pressed and event.button_index == MOUSE_BUTTON_LEFT:
#		# Link clicked on
#		if $Rect.get_global_rect().has_point(get_global_mouse_position()):
#			$Rect.color = Color(1, 0, 0, 1)
#			return
#
#		# Nothing clicked on
#		$Rect.color = Color(1, 1, 1, 1)
