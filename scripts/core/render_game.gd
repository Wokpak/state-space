class_name GameRender extends Node

# Load scenes
const PlanetRenderScene = preload("res://scenes/RenderPlanet.tscn")
const LinkRenderScene = preload("res://scenes/RenderLink.tscn")

# Specify game engine
var game_engine : GameEngine

# Setup -----------------------------------------

func _init(engine : GameEngine):
	"""
	Initialise object with pointer to game engine
	
	:param engine: [GameEngine] The game engine
	"""
	# Assign game engine
	self.game_engine = engine

func render():
	"""
	Render game engine. Note: This only needs to be called once
	"""
	# Determine player colours
	var colours : Array
	colours.append(Config.get_value("game", "colour_player1"))
	colours.append(Config.get_value('game', 'colour_player2'))
	colours.append(Config.get_value('game', 'colour_player3'))
	colours.append(Config.get_value('game', 'colour_player4'))
	var player_colours : Dictionary = {'Neutral': Config.get_value("game", "colour_neutral")}
	for ii in range(len(self.game_engine.players)):
		var player = self.game_engine.players[ii]
		var colour = colours[ii]
		player_colours[player] = colour
		
	# Set player colours for all planet_render objects
	PlanetRender.player_colours = player_colours

	# Render planets
	for planet_name in game_engine.vertices:
		# Get planet object
		var planet = game_engine.get_vertex(planet_name)
		# Create planet render
		var planet_render = PlanetRenderScene.instantiate()
		planet_render.initialise(planet)
		self.add_child(planet_render)

	# Render links
	for planet_name in game_engine.vertices:
		# Get planet object
		var planet = game_engine.get_vertex(planet_name)
		# Get links
		var links = planet.get_edges()
		# Create link render(s)
		for link in links:
			var link_render = LinkRenderScene.instantiate()
			link_render.initialise(planet, link)
			self.add_child(link_render)

func close():
	"""Remove all rendered objects and free self"""
	# Remove planets and links
	var children = get_children()
	for child in children:
		child.free()

	# Free self
	self.queue_free()
