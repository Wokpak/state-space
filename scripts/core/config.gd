extends Node

# Singleton instance
var _instance = null
var config : ConfigFile

func _ready():
	self.config = ConfigFile.new()
	var err = self.config.load("res://config/config.cfg")
	if err == OK:
		print("Config loaded successfully")
	else:
		print("Failed to load config:", err)

	# Store the configuration values
	_instance = self

func get_value(section_name, setting_name, default_value = null):
	return config.get_value(section_name, setting_name, default_value)
	
func set_value(section_name, setting_name, value):
	config.set_value(section_name, setting_name, value)
