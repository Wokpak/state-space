class_name PlanetRender extends ColorRect

## Player colours (defined for all instances of planet_render)
static var player_colours : Dictionary
## The planet object this scene will render
var planet: Planet
## The link render objects
var links: Dictionary

# Signals
signal selected

# -----------------------------------------------

func initialise(plan: Planet):
	# Link planet object
	self.planet = plan
	
	# Set node name to planet id
	self.name = self.planet.id
	
	# Set planet render size
	var planet_scale = Config.get_value("planet", "planet_scale")
	self.size = Vector2(planet_scale, planet_scale) * self.planet.size
	
	# Set planet render position (relative to top left corner)
	self.position = self.planet.position - (self.size / 2)

	# Set planet render material
	self.material = self.material.duplicate()
	var colour = self.player_colours[self.planet.empire]
	self.material.set_shader_parameter('colour_empire', colour)

	# Set info panel position (relative to planet render position)
	$Panel.position = Vector2(self.size[0] + 10, 0)

	# Set up panel
	$Panel/Info/HBox/Margin/Vbox/CenterContainer.visible = true
	$Panel/Info/HBox/Margin/Vbox/Name.text = self.planet.id
	
	# Update planet values
	self.update_values()

# Old code --------------------------------------

func _input(event):
	# Nothing selected
	if not event is InputEventMouseButton:
		return
	if not event.pressed:
		return
	if not event.button_index == MOUSE_BUTTON_LEFT:
		return

	var rect = self.get_global_rect().has_point(get_global_mouse_position())

	# Planet selected
	if self.get_global_rect().has_point(get_global_mouse_position()):
		emit_signal('selected', self.parent)
		return

	# Info panel clicked on
	var panel_rect = self.get_rect()
	panel_rect.position = panel_rect.position + $Panel.position
	panel_rect.size = $Panel/Info.get_rect().size
	if panel_rect.has_point(get_global_mouse_position()):
		return

func update_values():
	# Set planet colour based on empire
	var colour = self.player_colours[self.planet.empire]
	self.material.set_shader_parameter('empire_colour', colour)

	# Update planet description text
	var summary_node = $Panel/Info/HBox/Margin/Vbox/CenterContainer
	summary_node.get_node("Forces/Label").text = str(self.planet.forces)
	summary_node.get_node("Colony/Label").text = str(self.planet.colony)
	summary_node.get_node("Defense/Label").text = str(self.planet.defences)

func link_client(client):
	"""Link planet signals to client"""
	self.connect('selected', Callable(client, '_on_planet_selected'))
