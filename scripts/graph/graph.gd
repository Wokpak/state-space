class_name Graph extends RefCounted
"""
Omni-directional graph data structure formed of vertices and edges

Nomenclature
- Vertex
- Edge
"""

var vertices: Dictionary

func _init():
	vertices = {}

# Construction ----------------------------------

func add_vertex(vertex: GraphVertex):
	"""Add vertex to graph"""
	if vertex.id not in vertices:
		vertices[vertex.id] = vertex
	else:
		push_error("Vertex with ID '%s' already in graph!" % vertex.id)

func get_vertex(vertex_id: String) -> GraphVertex:
	"""Return named vertex from graph"""
	return vertices.get(vertex_id, null)

func add_edge(vertex_id1: String, vertex_id2: String):
	"""Add edge between vertices"""
	var vertex1 = self.get_vertex(vertex_id1)
	var vertex2 = self.get_vertex(vertex_id2)

	if vertex1 and vertex2:
		vertex1.add_edge(vertex2)
		vertex2.add_edge(vertex1) # For omni-directional graph
	else:
		push_error("One or both vertices do not exist: %s, %s" % [vertex_id1, vertex_id2])

func get_all_edges() -> Array:
	"""Find all (unique) edges in the graph"""
	var links = []
	var seen_links = {}
	
	for vertex in self.vertices.values():
		for edge in vertex.edges.values():
			var link = [vertex, edge]
			var reverse_link = [edge, vertex]
			if not seen_links.has(reverse_link) and not seen_links.has(link):
				links.append(link)
				seen_links[link] = true
				seen_links[reverse_link] = true

	return links

# Analysis --------------------------------------

func get_isolated_vertices() -> Array:
	"""Return verticies with no edges"""
	var isolated_vertices : Array = []
	for vertex in vertices.values():
		if vertex.is_isolated():
			isolated_vertices.append(vertex)
	return isolated_vertices
	
func get_non_isolated_vertices() -> Array:
	"""Return verticies with any edges"""
	var connected_vertices : Array = []
	for vertex in vertices.values():
		if not vertex.is_isolated():
			connected_vertices.append(vertex)
	return connected_vertices
	
func get_leaf_vertices() -> Array:
	"""Return verticies with one edge"""
	var isolated_vertices : Array = []
	for vertex in vertices.values():
		if vertex.is_leaf():
			isolated_vertices.append(vertex)
	return isolated_vertices
	
func get_branch_vertices() -> Array:
	"""Return verticies with more than one edge"""
	var branch_vertices : Array = []
	for vertex in vertices.values():
		if vertex.is_branch():
			branch_vertices.append(vertex)
	return branch_vertices

func get_edge_vertices(vertex: GraphVertex) -> Array:
	"""Get edge vertices for given vertex"""
	return vertex.edges.values()

func get_non_edge_vertices(vertex: GraphVertex, vertices : Array = self.vertices.values()) -> Array:
	"""Get non edge vertices for given vertex"""
	# Get edge vertices
	var edge_vertices = self.get_edge_vertices(vertex)
	# Filter to leave only non-edge vertices
	var non_edge_vertices = vertices.filter(func(vertex): return vertex not in edge_vertices)
	return non_edge_vertices

func get_reachable_vertices(vertex: GraphVertex) -> Array:
	"""Get all reachable vertices from a given vertex (using flood fill)"""
	return vertex.get_reachable_vertices()

func get_unreachable_vertices(vertex: GraphVertex) -> Array:
	"""Get all vertices uncreachable from given vertex"""
	# Get reachable
	var reachable = self.get_reachable_vertices(vertex)
	
	# Get all verticies and filter unreachable
	var unreachable = self.vertices.values()
	unreachable.erase(vertex)
	unreachable = unreachable.filter(func(vert): return vert not in reachable)
	return unreachable

func find_path(start : GraphVertex, end : GraphVertex, vertices : Array = self.vertices.values()):
	"""Find path between vertices using Uniform Cost Search version of Dijkstra's Algorithm"""
	#vertices = vertices.duplicate()

	var vertex : GraphVertex = start
	var frontier : Array = [vertex]
	var expanded : Array = []
	var dists : Dictionary = {vertex: 0}
	var parents : Dictionary = {vertex: null}

	while not end in parents:
		# Check frontier empty
		if len(frontier) == 0:
			break
		# Get new vertex and add to expanded
		vertex = frontier.pop_front()
		expanded.append(vertex)
		# Get edges
		var edges = self.get_edge_vertices(vertex).duplicate()
		for edge in edges:
			# Not yet expanded
			if not edge in dists:
				dists[edge] = dists[vertex] + 1
				parents[edge] = vertex
			# Shorter path found
			if dists[edge] > dists[vertex] + 1:
				dists[edge] = dists[vertex] + 1
				parents[edge] = vertex
			# Add links to frontier if not expanded
			if expanded.find(edge) == -1:
				frontier.append(edge)
		# Add vertex to visited
		expanded.append(vertex)

	# Check end found
	if not end in parents:
		return []

	# Get path between start and end
	var linkpath = [end]
	while not start in linkpath:
		vertex = parents[linkpath[-1]]
		linkpath.append(vertex)
	linkpath.reverse()

	return linkpath
