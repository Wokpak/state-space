extends GutTest

func test_vertex():
	# Create vertex
	var vertex = GraphVertex.new('vertex')
	
	# Check id set correctly
	assert_true(vertex.id == 'vertex')
	
	# Check it has no edges
	assert_true(len(vertex.edges.keys()) == 0)
	
	# Check logic functions
	assert_true(vertex.is_isolated())
	assert_false(vertex.is_leaf())
	assert_false(vertex.is_branch())
	
	var vertex1 = GraphVertex.new('vertex1')
	vertex.add_edge(vertex1)
	assert_false(vertex.is_isolated())
	assert_true(vertex.is_leaf())
	assert_false(vertex.is_branch())
	
	var vertex2 = GraphVertex.new('vertex2')
	vertex.add_edge(vertex2)
	assert_false(vertex.is_isolated())
	assert_false(vertex.is_leaf())
	assert_true(vertex.is_branch())

func test_graph():
	# Create graph
	var graph = Graph.new()
	
	# Check get functions with no vertices
	var edges = graph.get_isolated_vertices()
	assert_true(edges.size() == 0)
	edges = graph.get_non_isolated_vertices()
	assert_true(edges.size() == 0)
	edges = graph.get_leaf_vertices()
	assert_true(edges.size() == 0)
	edges = graph.get_branch_vertices()
	assert_true(edges.size() == 0)
	
	# Create vertices manually and add to graph
	var vertex0 = GraphVertex.new('vertex0')
	var vertex1 = GraphVertex.new('vertex1')
	var vertex2 = GraphVertex.new('vertex2')
	graph.add_vertex(vertex0)
	graph.add_vertex(vertex1)
	graph.add_vertex(vertex2)
	
	# Retrieve vertices and test they are the same
	var vertexn = graph.get_vertex('vertex0')
	assert_same(vertex0, vertexn)
	vertexn = graph.get_vertex('vertex1')
	assert_same(vertex1, vertexn)
	vertexn = graph.get_vertex('vertex2')
	assert_same(vertex2, vertexn)

	# Check get functions with three isolated verticies
	edges = graph.get_isolated_vertices()
	assert_true(edges.size() == 3)
	edges = graph.get_non_isolated_vertices()
	assert_true(edges.size() == 0)
	edges = graph.get_leaf_vertices()
	assert_true(edges.size() == 0)
	edges = graph.get_branch_vertices()
	assert_true(edges.size() == 0)

	
	# Add edge to graph
	graph.add_edge('vertex0', 'vertex1')
	
	# Check vertex references now have connection
	assert_has(vertex0.edges, 'vertex1')
	assert_has(vertex1.edges, 'vertex0')
	edges = vertex0.get_edges()
	assert_has(edges, vertex1)
	edges = vertex1.get_edges()
	assert_has(edges, vertex0)
	
	# Check get functions with two connected verticies
	edges = graph.get_isolated_vertices()
	assert_true(edges.size() == 1)
	assert_same(edges[0], vertex2)
	edges = graph.get_non_isolated_vertices()
	assert_true(edges.size() == 2)
	edges = graph.get_leaf_vertices()
	assert_true(edges.size() == 2)
	edges = graph.get_branch_vertices()
	assert_true(edges.size() == 0)
	
	# Add additional edge to graph
	graph.add_edge('vertex1', 'vertex2')
	
	# Check get functions with three connected verticies
	edges = graph.get_isolated_vertices()
	assert_true(edges.size() == 0)
	edges = graph.get_non_isolated_vertices()
	assert_true(edges.size() == 3)
	edges = graph.get_leaf_vertices()
	assert_true(edges.size() == 2)
	edges = graph.get_branch_vertices()
	assert_true(edges.size() == 1)
	assert_same(edges[0], vertex1)

func test_vertex_reachable():
	# Create graph
	var graph = Graph.new()
	# Add verticies
	var vertex0 = GraphVertex.new('vertex0')
	var vertex1 = GraphVertex.new('vertex1')
	var vertex2 = GraphVertex.new('vertex2')
	var vertex3 = GraphVertex.new('vertex3')
	var vertex4 = GraphVertex.new('vertex4')
	var vertex5 = GraphVertex.new('vertex5')
	graph.add_vertex(vertex0)
	graph.add_vertex(vertex1)
	graph.add_vertex(vertex2)
	graph.add_vertex(vertex3)
	graph.add_vertex(vertex4)
	graph.add_vertex(vertex5)

	# Add edges
	graph.add_edge('vertex0', 'vertex1')
	graph.add_edge('vertex0', 'vertex2')
	graph.add_edge('vertex2', 'vertex3')
	graph.add_edge('vertex4', 'vertex5')

	var reachable = graph.get_reachable_vertices(vertex0)
	assert_true(reachable.size() == 3)
	assert_has(reachable, vertex1)
	assert_has(reachable, vertex2)
	assert_has(reachable, vertex2)

	var unreachable = graph.get_unreachable_vertices(vertex0)
	assert_true(unreachable.size() == 2)
	assert_has(unreachable, vertex4)
	assert_has(unreachable, vertex5)

func test_get_all_edges():
	# Create graph
	var graph = Graph.new()

	# Add verticies
	var vertex0 = GraphVertex.new('vertex0')
	var vertex1 = GraphVertex.new('vertex1')
	var vertex2 = GraphVertex.new('vertex2')
	var vertex3 = GraphVertex.new('vertex3')
	var vertex4 = GraphVertex.new('vertex4')
	var vertex5 = GraphVertex.new('vertex5')
	graph.add_vertex(vertex0)
	graph.add_vertex(vertex1)
	graph.add_vertex(vertex2)
	graph.add_vertex(vertex3)
	graph.add_vertex(vertex4)
	graph.add_vertex(vertex5)

	# Add edges
	graph.add_edge('vertex0', 'vertex1')
	graph.add_edge('vertex0', 'vertex2')
	graph.add_edge('vertex2', 'vertex3')
	graph.add_edge('vertex3', 'vertex0')
	graph.add_edge('vertex4', 'vertex5')

	var edges = graph.get_all_edges()
	assert_true(len(edges) == 5)
