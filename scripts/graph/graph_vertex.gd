class_name GraphVertex extends RefCounted

var id: String
var edges: Dictionary

func _init(vertex_id: String):
	self.id = vertex_id
	self.edges = {}

# Construction ----------------------------------

func add_edge(vertex: GraphVertex):
	"""Add edge to vertex"""
	edges[vertex.id] = vertex

func get_edges() -> Array:
	"""Return all edge vertices"""
	return edges.values()

func get_reachable_vertices() -> Array:
	"""Get all reachable vertices (using flood fill)"""
	var reachable : Array = [self]
	var to_see : Array = self.get_edges()

	# Get edges
	while len(to_see) > 0:
		# Get reachable vertex
		var vertex = to_see.pop_front()
		# Add to reachable
		reachable.append(vertex)
		# Append new edges
		to_see = to_see + vertex.get_edges()
		# Remove reachable planets from to_see list
		to_see = to_see.filter(func(vert): return vert not in reachable)
	
	# Remove self and return
	reachable.erase(self)
	return reachable

# Logic -----------------------------------------

func is_isolated() -> bool:
	"""Return true if vertex has no edges"""
	return self.edges.size() == 0

func is_leaf() -> bool:
	"""Return true if vertex has one edge"""
	return self.edges.size() == 1
	
func is_branch() -> bool:
	"""Return true if vertex has more than one edge"""
	return self.edges.size() > 1

#func can_reach(vertex: GraphVertex) -> bool:
	#"""Return true if connected to"""
	## Add code here
