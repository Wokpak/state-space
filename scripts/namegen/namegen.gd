class_name NameGenerator

const consts = 'bcdfghjklmnpqrstvwxyz'
const vowels = 'aeiou'
const double_consts = {
	'b': 'lr',
	'c': 'lhr',
	'd': 'r',
	'f': 'lr',
	'g': 'lrh',
	'h': '',
	'j': '',
	'k': 'hrn',
	'l': '',
	'm': '',
	'n': '',
	'p': 'rnl',
	'q': 'w',
	'r': '',
	's': 'chklnptwy',
	't': 'r',
	'v': '',
	'w': 'r',
	'x': '',
	'y': '',
	'z': '',
}

static func gen_name_randlen(min_letters: int, max_letters: int, rng: RandomNumberGenerator = null) -> String:
	"""
	Generate random name of random length within bounds
	
	:param min_letters: [int] Min number of letters in name
	:param max_letters: [int] Max number of letters in name
	:returns: [String] Name
	"""
	# Initialise rng
	if rng == null:
		rng = RandomNumberGenerator.new()

	# Determine number of letters
	var nletters = rng.randi_range(min_letters, max_letters)

	# Generate name
	return gen_name(nletters, rng)

static func gen_name(nletters: int, rng: RandomNumberGenerator = null) -> String:
	"""
	Generate name of given length
	:param nletters: [int] Number of letters
	:returns: [String] Name
	"""
	# Initialise rng
	if rng == null:
		rng = RandomNumberGenerator.new()
		# rng.randomize()

	# Define vars
	var name = ''
	var valid_letters = ''
	var ind : int

	# Get additional letters
	while len(name) < nletters:
		valid_letters = _get_valid_letters(name)
		ind = rng.randi_range(0, len(valid_letters) - 1)
		name = name + valid_letters[ind]

	# Capitalise and return
	name = name[0].to_upper() + name.substr(1, -1)
	return name

static func _get_valid_letters(name: String):
	"""Return string containing valid following letters"""
	# No letters
	if len(name) == 0:
		return consts + vowels

	# Ends in vowel
	if name[-1] in vowels:
		return consts

	# Single letter and is consts
	if len(name) == 1 and name[-1] in consts:
		return vowels + double_consts[name[-1]]

	# Multiple letters and final is consts
	if name.substr(-1, -2) in consts:
		return vowels

	return vowels # + double_consts[name[-1]]
